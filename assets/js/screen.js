//I declare an array of the colours that the box will shift too
let colours = ["green", "blue", "red", "orange", "pink" ]
//Sets up an array that the box size will randomly select when clicked
let size = [ "200px", "500px", "600px", "10px", "100px", "700px"]
let box = document.getElementById("box");
//Calls a function to make the box change colour when clicked
box.addEventListener ("click", changeBoxColour)
//Calls a function to change the box's size when clicked
box.addEventListener("click", changeBoxSize)



//The function that changes the colour of the box for every time it is clicked and returns a new colour
function changeBoxColour()
{
    let newColour = colours[Math.floor(Math.random() * colours.length)];
    console.log("new box colour is:")
    console.log(newColour);
    box.style.backgroundColor = newColour;
}


//Changes the size of the box to a random size predetermined by the array
function changeBoxSize()
{
    //randomly selects a number from the arrays
    let newwidth = size[Math.floor(Math.random() * size.length)];
    //makes the length the same as the width to make sure the box remains box
    let newlength = newwidth;
    console.log("new box size is:")
    console.log(newwidth);
    console.log(newlength);
    box.style.width= newwidth;
    box.style.height= newlength;
}

